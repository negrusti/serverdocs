Quick reference for your Ubuntu server
========

Hostname
--------

Every server will have a hostname assigned to it. It may or may not coincide with one of the websites hosted on this server.

Example: do.mysitedomain.com, linode.myothersitedomain.com, server1.site.com

Froxlor Control Panel
---------------------

| Accessible via https://<your server hostname>
| Login as admin to add new domains
| Login as another user to manage databases and email addresses

SSL Certificates
----------------

Certificates are obtained from letsencrypt.org and renewed automatically. Logged in as admin into Froxlor, you can see certificate status in the "SSL Certificates" in the left panel.

Databases
---------

Login into Froxlor as regular user, click on "Databases" in the left panel.


PHPMyAdmin
----------

Accessible via https://<your server hostname>/phpmyadmin

SFTP
----

| Host: <your server hostname>
| User: <your froxlor user> - not admin
| Password: <your froxlor user password>

Email accounts
--------------

If your mail resides on your Ubuntu server, mail accounts are managed via Froxlor. Login as regular user (not admin),
select Addresses or Create email-address in the left panel menu. Created addresses can be full accounts
(messages are stored on the server, and you can send/receive messages via this server using email/password), or can be just forwarders to some other email address.

SMTP
----

| Host: <your server hostname>
| Port: 465 or 587
| Use TLS/SSL
| Username: <full email address>

IMAP
----

| Host: <your server hostname>
| Port: 993
| Use TLS/SSL
| Username: <full email address>

File locations
--------------

| Sites: /var/customers/webs/
| Site logs: /var/customers/logs/
| MySQL backups: /backups/mysql/

CloudFlare
----------

If your site uses CloudFlare, all DNS records for the domain and it's subdomains are managed on CloudFlare control panel.

Server Monitoring
-----------------

We use our own internal Zabbix monitoring installation for most of the servers we manage. We can grant access to our clients on request. A good simple alternative you can use is StatusCake.com, free plan is good enough for most.

Performance Testing
-------------------

GTMetrix.com

Email Deliverability Testing
----------------------------

mail-tester.com
